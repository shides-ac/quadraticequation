<head>
  <meta charset="utf-8">
  <title>Quadratic Equation :)</title>
  <meta name="description" content="This is the best portal to know EVERYTHING about the quadratic equation, enjoy!!!">
  <meta name="author" content="uniCTf_T34M">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">

  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/skeleton.css">
  <link rel="stylesheet" href="css/style.css">

  <link rel="icon" type="image/png" href="images/favicon.png">
</head>