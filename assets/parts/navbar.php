<div class="container">
    <nav class="navbar">
      <div class="container">
        <ul class="navbar-list">
          <li class="navbar-item" style="">
            <a class="navbar-link" href="/">Home</a>
          </li>
          <li class="navbar-item">
            <a class="navbar-link" href="/quadratic-equation.php">Quadratic Equation</a>
          </li>
          <?php
              if (isset($_SESSION['username'])) {
                // Showing a logout link
                print("<li class=\"profile navbar-item\">");
                print("<form action=\"/logout.php\" id=\"form-login-logout\">");
                print("<input class=\"button-primary\" type=\"submit\" value=\"Logout\" id=\"logout-btn\"/>");
                print("</form>");
                print("</li>");
                // Showing a link to user-profile
                print("<li class=\"profile navbar-item\">");
                printf("<a class=\"navbar-link\" href=\"profile.php\" style=\"text-transform:none;\">%s</a>", $_SESSION["username"]);
                print("</li>");
              } else {
                print("<li class=\"profile navbar-item\">
                        <a class=\"navbar-link\" href=\"login.php\">Login</a>
                       </li>");
              }
          ?>
        </ul>
      </div>
    </nav>
  </div>