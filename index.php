<!DOCTYPE html>
<html lang="en">
  <?php 
    include("config.php");
    include("assets/parts/head.php");
  ?>
  <body>
    <?php 
      $DEBUG = false;
      if ($DEBUG) {
        $_SESSION["username"]="debug_user";
      }

      include("assets/parts/navbar.php");
    ?>

    <div class="container" style="padding-bottom: 20rem;">
      <div class="row">
        <div class="one-half column" style="margin-top: 25%">
          <h4>Hello!</h4>
          <p>Do you know what is a quadratic equation?<br>
            I <i><b>hope</b></i> you know what they are but if you never heard anything about them or 
            you want to improve your knowledge of quadratic equation, <b>you're in the right place!!!</b></br>
            Look to the right, there's a shot of what a quadratic equation is ;)
          </p>
        </div>
        <div class="one-half column" style="margin-top: 30%">
          <img src="assets/images/quadratic_equation.png"/>
        </div>
      </div>
    </div>
  </body>
  <?php 
    include("assets/parts/footer.php");
  ?>
</html>
