<!DOCTYPE html>
<html>
    <?php
        include("config.php");
        include("assets/parts/head.php");

        if(isset($_POST["current-password"]) && isset($_POST["new-password"]) && isset($_POST["new-password-retype"])){
            $newpassword = sha1($_POST["new-password"]);
            $newpwdretype = sha1($_POST["new-password-retype"]);
            $oldpassword = sha1($_POST["current-password"]);
            $username = $_SESSION["username"];
            if($newpassword != $newpwdretype){
                $_SESSION["change-fail"] = true;
                $_SESSION["motivation"] = "New password and its retype mismatch";
            } else {
                $sql = "UPDATE Users SET password='$newpassword' WHERE username='$username' AND password='$oldpassword'";
                $result = mysqli_query($conn, $sql);
                if($result){
                    $prepare = mysqli_prepare($conn, "SELECT * FROM Users WHERE password=? AND username=?;");
                    $prepare->bind_param("ss", $newpassword, $username);
                    $res = $prepare->execute();
                    if ($res) {
                        $res_prepare = $prepare->get_result();
                        if (mysqli_num_rows($res_prepare)) {
                            $_SESSION["change-fail"] = false;
                            unset($_SESSION["motivation"]);
                        } else {
                            $_SESSION["change-fail"] = true;
                            $_SESSION["motivation"] = "Current password is incorrect";
                        }
                    } else {
                        $_SESSION["change-fail"] = true;
                        $_SESSION["motivation"] = "Error executing query.";
                    }
                } else {
                    $_SESSION["change-fail"] = true;
                    $_SESSION["motivation"] = "Error executing query.";
                }
            }
        }
    ?>
    <body>
        <?php
            include("assets/parts/navbar.php");
            if(!isset($_SESSION["username"])){
                print '
                <div class="container" style="margin-top: 5%; text-align: center; padding-bottom:20rem;">
                    <div class="row">
                        <div class="three-quarter column">
                            <h4>Error</h4></br>
                            <p>You\'ve to login in order to view your profile.
                            </br><i>Redirecting to home page...</i></p>
                        </div>
                    </div>
                    <script type="text/javascript">
                        $(document).ready(function(){
                            setTimeout(function(){
                                window.location.href = "index.php";
                            }, 3000);
                        });
                    </script>
                </div>
                ';
            } else {
                print '
                <div class="container" style="margin-top:5%; padding-bottom:10rem">
                    <div class="row">
                        <div class="three-quarter columns" style="text-align:center">
                            <div class="container">
                                <div class="row">
                                    <h4 class="u-pull-left" style="font-weight:800;">Profile</h4>
                                </div>
                                <div class="row">
                                    <div class="six columns">
                                        <label for="par-username" class="u-pull-left">Username:</label>
                                        <p id="par-username" class="u-pull-right">'.$_SESSION["username"] .'</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="six columns">
                                        <label for="par-biography" class="u-pull-left">Your Biography: </label>
                                        <p id="par-biography" class="u-pull-right">' .$_SESSION["bio"] . '</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="twelve columns" style="text-align:center">
                            <div class="container">
                                <div class="row">
                                    <h4 class="u-pull-left" style="font-weight:800">Change Password</h4>
                                </div>
                                <div class="row">
                                    <form method="post">
                                        <div class="row">
                                            <div class="eight columns">
                                                <label for="input-cur-pwd" class="u-pull-left">Current Password</label>
                                                <input class="u-full-width" type="password" id="input-cur-pwd" name="current-password" placeholder="Current password"/>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="four columns">
                                                <label for="input-new-pwd" class="u-pull-left">New Password</label>
                                                <input class="u-full-width" type="password" id="input-new-pwd" name="new-password" placeholder="New password"/>
                                            </div>
                                            <div class="four columns">
                                                <label for="input-new-pwd-retype" class="u-pull-left">Retype the new password</label>
                                                <input class="u-full-width" type="password" id="input-new-pwd-retype" name="new-password-retype" placeholder="Retype new password"/>
                                            </div>
                                            <div class="two columns">
                                                <input id="input-submit" type="submit" value="Change Password" style="margin-top:3rem" disabled/>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="row">
                                    <div class="twelve columns">';
                                    if(isset($_SESSION["change-fail"])){
                                        if(!$_SESSION["change-fail"]){
                                            print '<p style="text-color:green;font-weight:bold">Password changed correctly!</p>';
                                            unset($_SESSION["change-fail"]);
                                            unset($_SESSION["motivation"]);
                                        } else {
                                            print '<p style="display:inline;color:red;font-weight:bold;">Error updating password! <h6><i>( '.$_SESSION["motivation"]. ')</i></h6></p>';
                                            unset($_SESSION["change-fail"]);
                                            unset($_SESSION["motivation"]);
                                        }
                                    }
                                print '
                                    </div>
                                </div>
                                <script type="text/javascript">
                                        $(document).ready(function(){
                                            var cur_pwd = $("#input-cur-pwd"),
                                                new_pwd = $("#input-new-pwd"),
                                                new_pwd_retype = $("#input-new-pwd-retype"),
                                                submit_btn = $("#input-submit");

                                            var to_enable = [false, false];
                                            var activate_submit = function (){
                                                $(submit_btn).addClass("button-primary");
                                                $(submit_btn).prop("disabled", false);
                                            };
                                            var disable_submit = function() {
                                                $(submit_btn).removeClass("button-primary");
                                                $(submit_btn).prop("disabled", true);
                                            };
                                            setInterval(function(){
                                                if(to_enable[0] && to_enable[1]){
                                                    activate_submit();
                                                } else {
                                                    disable_submit();
                                                }
                                            }, 100);

                                            var compare_pwd = function(pwd1, pwd2){
                                                if(pwd1 != ""){
                                                    if(pwd1 == pwd2)
                                                        to_enable[1] = true;
                                                    else 
                                                        to_enable[1] = false;
                                                } else 
                                                    to_enable[1] = false;
                                            };

                                            $(cur_pwd).on("input", function(){
                                                if($(this).val() != "")
                                                    to_enable[0] = true;
                                                else 
                                                    to_enable[0] = false;
                                            });

                                            $(new_pwd).on("input", function(){
                                                compare_pwd($(this).val(), $(new_pwd_retype).val());
                                            });

                                            $(new_pwd_retype).on("input", function(){
                                                compare_pwd($(this).val(), $(new_pwd).val());
                                            });

                                        });
                                    </script>
                            </div>    
                        </div>
                    </div>
                </div>';
            }
        ?>
    </body>
    <?php
        include("assets/parts/footer.php");
    ?>
</html>