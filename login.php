<!DOCTYPE html>
<html lang="en">
    <?php 
        include("config.php");
        include("assets/parts/head.php");

        $DEBUG=false;
        if($DEBUG) {
            $_SESSION["username"] = "debug_user";
        }

        // login handling
        if(isset($_POST["username"]) && isset($_POST["password"])){
            
            if ($prepared = mysqli_prepare($conn, "SELECT * FROM Users WHERE username=? AND password=?")){
                
                $prepared->bind_param("ss", $_POST["username"], sha1($_POST["password"]));
                
                $prepared->execute();
                $result = $prepared->get_result()->fetch_all(MYSQLI_ASSOC);
                if(!$result) $_SESSION["login_fail"] = true;
                else {
                    foreach($result as $key => $row){
                        $_SESSION["id"] = $row["id"];
                        $_SESSION["username"] = $row["username"];
                        $_SESSION["bio"] = $row["bio"];
                    }
                    unset($_SESSION["login_fail"]);
                }
                mysqli_free_result($result);
                $prepared->close();
            }
        }
    ?>
    <body>
        <div class="container" style="padding-bottom:20rem;">
            <div class="row">
                <?php 
                    include("assets/parts/navbar.php");

                    if(isset($_SESSION["login_fail"])){
                        print '<div class="twelve columns" style="text-align:center;margin-top:10%">
                                <h3>Login Failed :(</h3>
                                <p><a href="login.php">Retry</a> or go to <a href="index.php">Home page</a></p>';
                        
                        session_destroy();
                    } else {
                        if(isset($_SESSION["username"])) {
                            print '
                                <div class="twelve columns" style="text-align:center;margin-top:10%">
                                    <h4>Hi ' . $_SESSION["username"] . '!</h4><br/>';
                            if (!isset($_POST["username"]))
                                print '<p style="display:inline;">You\'re already logged in so you\'ll be redirect to home page in <b id="counter">5</b> <p id="seconds" style="display:inline">seconds</p></p>';
                            else
                                print '<p style="display:inline;">You\'ll be redirect to home page in <b id="counter">5</b> <p id="seconds" style="display:inline">seconds</p></p>';
                            
                            print '
                                </div>
                                <script type="text/javascript">
                                    var counter = $("#counter");
                                    setInterval(function(){
                                        var sec = Number(counter.html());
                                        counter.html(sec - 1 );
                                        if(sec == 2)
                                            $("#seconds").html("second");
                    
                                        if(sec == 1)
                                            window.location.href="index.php";
                                    }, 1000); 
                                </script>';
                        } else {
                            print '
                            <div class="twelve columns" style="text-align:center; margin-top:10%">
                                <h4>Login</h4>
                                <form method="post" style="margin-left:30%;margin-right:30%;">
                                    <div class="row">
                                        <div class="twelve columns">
                                            <label for="input-username" style="float:left">Username</label>
                                            <input type="text" class="u-full-width" placeholder="Insert username" id="input-username" name="username"/>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="twelve columns">
                                            <label for="input-password" style="float:left">Password</label>                        
                                            <input type="password" class="u-full-width" placeholder="Insert password" id="input-password" name="password"/>
                                        </div>
                                    </div>
                                    <div class="row" style="text-align:center;">
                                        <div class="twelve columns">
                                            <input class="button-primary u-full-width" type="submit" value="Login"/>
                                        </div>
                                    </div>
                                </form>
                                <p><i>New User? Register <a href="register.php" style="text-style: bold">Here!</a> :) </i></p>
                                </div>
                            </div>
                            ';
                        }
                    }
                ?>

            </div>
        </div>
    </body>
    <?php 
        include("assets/parts/footer.php");
    ?>
</html>