<!DOCTYPE html>
<html>
    <?php 
        include("assets/parts/head.php");
        if(!isset($_SESSION["username"])) header("Location:index.php");
    ?>
    <body>
        <?php
            include("assets/parts/navbar.php");
        ?>
        <div class="container" style="text-align:center; margin-top:10%;">
            <div class="row">
                <div class="twelve columns">
                    <h4> Bye <?php 
                        print $_SESSION["username"];
                        session_destroy();

                        ?> !</h4>
                    <p style="display:inline">Redirecting in <p style="display:inline;" id="seconds">5</p> seconds...</p>
                    <script type="text/javascript">
                        var second = $("#seconds");
                        var sec_int = Number(second.html());
                        setInterval(function() {
                            sec_int-=1;
                            second.html(sec_int);
                            if(sec_int == 0)
                                window.location.href = "index.php";
                        }, 1000);

                    </script>                    
                </div>
            </div>
        </div>
    </body>
    <?php
        include("assets/parts/footer.php");
    ?>
</html>
