<!DOCTYPE html>
<html>
    <?php
        include("assets/parts/head.php");
    ?>
    <body>
        <?php
            include("assets/parts/navbar.php");
            if (isset($_SESSION["username"])) {
                print '
                    <div class="container" style="margin-top:5%;">
                        <div class="row">
                            <div class="three-quarter column">
                                <h4>Introduction</h4>
                            <p>
                                In algebra, a quadratic equation (from the Latin quadratus for "square") is any equation having the form 
                                <b><i>ax^2+bx+c=0</i></b> where:
                                <ul>
                                    <li><b>x</b> - represents an unknown;</li>
                                    <li><b>a</b>, <b>b</b>, <b>c</b> - represents known number with <b> a ≠ 0</b>.</li>
                                </ul>
                                If <b>a = 0</b>, then the equation is <b>linear</b>, not quadratic, as there is no <b>ax^2</b> term.<br/>
                                The numbers <b>a</b>, <b>b</b>, and <b>c</b> are the coefficients of the equation and may be distinguished by calling them, respectively, <i><b>the quadratic coefficient</b></i>, <b><i>linear coefficient</i></b> and <b><i>the constant </i></b>/<b><i> free term</i></b>.<br/>
                                <br>The values of <b>x</b> that <b><i>satisfy</i></b> the equation are called <b><i>solutions of the equation</i></b>, and <b><i>roots</i></b> or <b><i>zeros</i></b> of its left-hand side.
                                <br>A quadratic equation has at most two solutions:
                                    <ul>
                                        <li>If there is <i>no real</i> solution, there are two <i>complex</i> solutions.</li>
                                        <li>If there is only <i>one</i> solution, one says that <i>it is a double root.</i></li>
                                    </ul>
                                So a quadratic equation has always two roots, if complex roots are considered, and if a double root is counted for two.
                                </br>If the two solutions are denoted <b>r</b> and <b>s</b> (possibly equal), one has <b>ax^2+bx+c=a(x-r)(x-s)</b>.
                                </br></br>Thus, the <i>process of solving a quadratic equation</i> is also called <i>factorizing</i> or <i>factoring</i>.
                                </br><i><b>Completing the square</b></i> is the <i>standard method</i> for that, which results in the <a href="#quadratic-formula">quadratic formula</a>, which express the solutions in terms of a, b, and c.
                                </br></br><i><b><a href="#graphing-method">Graphing</a></b></i> may also be used for getting an <i>approximate</i> value of the solutions.
                                <br><br>Solutions to problems that may be expressed in terms of quadratic equations were known as early as 2000 BC.
                                <br>Because the quadratic equation involves only one unknown, it is called "univariate".
                                <br><br>The quadratic equation only contains powers of x that are non-negative integers, and therefore it is a polynomial equation. 
                                In particular, it is a second-degree polynomial equation, since the greatest power is two.
                                </p>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 5rem;">
                            <div class="one-third column" id="quadratic-formula" style="text-align:center;">
                                <img id="quadratic-formula-img" src="assets/images/quadratic_formula.png"/>
                                <label for="quadratic-formula-img">The quadratic formula</label>
                            </div>
                            <div class="one-third column" id="graphing-method" style="float:right;text-align:center;">
                                <img id="graphing-method-img" src="assets/images/graphing_method.png"/>
                                <label for="graphing-method-img">Graphing Method</label>
                            </div>
                        </div>
                    </div>
                    ';
            } else {
                print '
                <div class="container" style="margin-top: 5%; text-align: center; padding-bottom:20rem;">
                    <div class="row">
                        <div class="three-quarter column">
                            <p><h3>Sorry :(</h3><br>
                            Only authenticated user can access this information!</p>
                        </div>
                    </div>
                </div>
                ';
            }
            ?>
            
                     
    </body>
    <?php 
        include("assets/parts/footer.php");
    ?>
</html>