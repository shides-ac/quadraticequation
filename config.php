<?php
    $db_host = "localhost";
    $db_user = "root";
    $db_password = "Password";
    $db_name = "quad_equations";

    $conn = mysqli_connect($db_host, $db_user, $db_password, $db_name);
    if (!$conn) {
        die("Failed to connect to the database");
    }
    $fd = fopen("./the_equation", "r") or die("Server Error");
    $to_insert = fgets($fd);
    fclose($fd);
    if(is_string($to_insert)){
        $pwd = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(10/strlen($x)) )),1,10);
        $pwd = sha1($pwd);
        $user = "Flag";

        $sql = "DELETE FROM Users WHERE username=?";
        if($prepare = mysqli_prepare($conn, $sql)) {
            $prepare->bind_param("s", $user);

            $prepare->execute();
            if(mysqli_affected_rows($conn) >= 0){
                $sql_new = "INSERT INTO Users (username, password, bio) VALUES (?, ?, ?);";
                if($prepared = mysqli_prepare($conn, $sql_new)) {
                    $prepared->bind_param("sss", $user, $pwd, $to_insert);
                    $prepared->execute();
                    if(!(mysqli_affected_rows($conn) > 0)){
                        die("please reload the page");
                    }
                }
            } else {
                die("please reload the page");
            }
        }
    }
?>