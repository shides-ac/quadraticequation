<!DOCTYPE html>
<html>
    <?php
        include("config.php");
        include("assets/parts/head.php");

        if(isset($_SESSION["username"])) header("Location: index.php");
        
        if(isset($_POST["username"]) && isset($_POST["password"]) && isset($_POST["retype-pwd"])){
            if($_POST["password"] != $_POST["retype-pwd"]){
                $_SESSION["register-fail"] = true;
            } else {
                if (isset($_POST["bio"]) && $_POST["bio"] != ""){
                    $prepared = mysqli_prepare($conn, "INSERT INTO Users (username, password, bio) VALUES (?, ?, ?);");
                    $prepared->bind_param("sss", $_POST["username"], sha1($_POST["password"]), $_POST["bio"]);
                } else {
                    $prepared = mysqli_prepare($conn, "INSERT INTO Users (username, password) VALUES (?, ?);");
                    $prepared->bind_param("ss", $_POST["username"], sha1($_POST["password"]));
                }
                if($prepared){
                    $prepared->execute();
                    
                    if($prepared->insert_id){
                        $_SESSION["register-fail"] = false;
                        $_SESSION["username"] = $_POST["username"];
                        $_SESSION["id"] = $prepared->insert_id;
                        $_SESSION["bio"] = $_POST["bio"];
                    } else {
                        $_SESSION["register-fail"] = true;
                        $_SESSION["why"] = "Username <b>" .$_POST["username"] ."</b> already exists!";
                    }
                } else {
                    $_SESSION["register-fail"] = true;
                    $_SESSION["why"] = "Generic error.";
                }
                
                $prepared->close();
            }
        }
    ?>
    <body>
        <?php
            include("assets/parts/navbar.php");

            if(!isset($_SESSION["register-fail"])) {
                print '
                    <div class="container" style="padding-bottom:10rem">
                        <div class="row">
                            <div class="ten columns" style="margin-top:10%;margin-left:10%">
                                <form method="post">
                                    <h4>Register to Quadratic Equations</h4>
                                    <div class="row">
                                        <div class="six columns">
                                            <label for="inpt-username">Username</label>
                                            <input class="u-full-width" type="text" placeholder="Username" id="input-username" name="username">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="six columns">
                                            <label for="input-password">Password</label>
                                            <input type="password" class="u-full-width" placeholder="Password" id="input-password" name="password">
                                        </div>
                                        <div class="six columns">
                                            <label for="input-retype-pwd">Re-Type Password</label>
                                            <input type="password" class="u-full-width" placeholder="Retype the password" id="input-retype-pwd" name="retype-pwd">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="twelve columns">
                                            <label for="input-biography">biography</label>
                                            <textarea class="u-full-width" placeholder="Hello I\'m ... and I like to... " id="input-biography" name="bio"></textarea>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <input disabled class="u-full-width" id="btn-register" type="submit" value="Register">
                                    </div>
                                </form>
                                <script type="text/javascript">
                                    $(document).ready(function(){
                                        var user = $(\'#input-username\');
                                        var bio = $(\'#input-biography\');
                                        var pass = $("#input-password");
                                        var pass_rt = $("#input-retype-pwd");
                                        var btn_submit = $("#btn-register");
                                        var to_enable = [false, false];

                                        var disable_submit = function(){
                                            $(btn_submit).prop("disabled", true);
                                            $(btn_submit).removeClass("button-primary");
                                        };
                                        var enable_submit = function(){

                                            $(btn_submit).prop("disabled", false);
                                            $(btn_submit).addClass("button-primary");
                                        };

                                        setInterval(function(){
                                            if(to_enable[0] && to_enable[1]){
                                                enable_submit();
                                            } else {
                                                disable_submit();
                                            }
                                        }, 100);

                                        var compare_passwords = function(pwd1, pwd2) {
                                            if (pwd1 == pwd2) {
                                                to_enable[1] = true;
                                            } else {
                                                to_enable[1] = false;
                                            }

                                        }
                                        
                                        $(user).on("input", function(){
                                            var username = $(this).val();
                                            if(username == "")
                                                to_enable[0] = false;
                                            else
                                                to_enable[0] = true;
                                        });

                                        $(pass).on("input", function(){
                                            compare_passwords($(this).val(), $(pass_rt).val());
                                        });
                                        $(pass_rt).on("input", function(){
                                            compare_passwords($(this).val(), $(pass).val());
                                        });
                                    });
                                </script>
                            </div>
                        </div>
                    </div>
                ';
            } else {
                print '
                    <div class="container" style="padding-bottom:10rem;">
                        <div class="row">
                            <div class="twelve columns" style="text-align:center;margin-top:10%;">';
                if($_SESSION["register-fail"] == true){
                    print '<h4>Registration failed :(</h4>
                            <h6>Error: <i> '. $_SESSION["why"] . ' </i></h6>
                            <p style="display:inline"><a href="register.php">Retry</a> or wait <p style="display:inline" id="seconds">5</p> seconds to autoreload</p>';
                    
                    session_destroy();
                } else {
                    print '<h4>Welcome into Quadratic Equations <b>' . $_SESSION["username"]. '</b></h4>
                            <p style="display:inline">Go to <a href="index.php">Home Page</a> or wait <p style="display:inline" id="seconds">5</p> seconds to be autoredirected</p>';
                }
                print '     </div>
                        </div>
                    </div>
                    <script type="text/javascript">
                        $(document).ready(function(){
                            var sec = $("#seconds");
                            var sec_int = Number($(sec).html());
                            setInterval(function(){
                                sec_int-=1;
                                $(sec).html(sec_int);
                                if(sec_int == 0)';
                                if($_SESSION["register-fail"])
                                    print 'window.location.href = "register.php";';
                                else 
                                    print 'window.location.href = "index.php";';
                print '
                            }, 1000);
                        });
                    </script>';
            }
        ?>
    </body>
    <?php
        include("assets/parts/footer.php");
    ?>
</html>